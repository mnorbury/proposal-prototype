FROM maven:3-alpine as builder

ARG BUILD_ID=Unknown

COPY . /code
WORKDIR /code

RUN mvn -Dbuild.id=$BUILD_ID install

FROM java:8-alpine
MAINTAINER Martin Norbury <mnorbury@nso.edu>

COPY --from=builder /code/target/*.jar /code/
WORKDIR /code

CMD ["java", "-jar", "proposals-0.0.1-SNAPSHOT.jar"]
