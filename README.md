# Proposal Prototype

To build the image using [docker-compose](https://docs.docker.com/compose/):-

    > docker-compose build
   
To run the image:-

    > docker-compose up -d
    
Then navigate to:-

    > curl http://localhost:8080/proposal

To view the logs:-

    > docker-compose logs -f

To stop the image:-

    > docker-compose stop
