package edu.nso.proposals;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableAutoConfiguration
@EnableJpaAuditing
public class ProposalServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(ProposalServiceApplication.class, args);
    }
}
