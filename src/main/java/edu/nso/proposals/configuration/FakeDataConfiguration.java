package edu.nso.proposals.configuration;

import edu.nso.proposals.model.Person;
import edu.nso.proposals.model.Proposal;
import edu.nso.proposals.model.ProposalState;
import edu.nso.proposals.repository.PersonRepository;
import edu.nso.proposals.repository.ProposalRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;

@Profile("fake")
@Configuration
public class FakeDataConfiguration implements CommandLineRunner {
    private final ProposalRepository proposalRepository;
    private final PersonRepository personRepository;

    @Inject
    public FakeDataConfiguration(ProposalRepository proposalRepository, PersonRepository personRepository) {
        this.proposalRepository = proposalRepository;
        this.personRepository = personRepository;
    }

    @Override
    public void run(String... args) {
        createProposal();
    }

    @Transactional
    void createProposal() {
        Person person1 = Person.builder().firstName("Martin").lastName("Norbury").affiliation("NSO").email("mnorbury@nso.edu").build();
        Person person2 = Person.builder().firstName("Joe").lastName("Zoller").affiliation("NSO").email("jzoller@nso.edu").build();
        personRepository.save(Arrays.asList(person1, person2));

        proposalRepository.save(createProposals(person1, person2));
    }

    private Iterable<Proposal> createProposals(Person... people) {
        return IntStream.range(1, 100).mapToObj(x -> createProposal(x, people)).collect(toList());
    }

    private Proposal createProposal(int instance, Person... people) {
        List<Person> peopleList = Arrays.asList(people);
        Collections.shuffle(peopleList);

        Proposal.ProposalBuilder proposalBuilder = Proposal.builder();
        proposalBuilder.title("My Proposal " + instance);
        proposalBuilder.revision(1);
        proposalBuilder.proposalAbstract("Something interesting");
        proposalBuilder.pi(peopleList.get(0));
        proposalBuilder.copi(peopleList.get(1));
        proposalBuilder.state(ProposalState.DRAFT);

        return proposalBuilder.build();
    }
}
