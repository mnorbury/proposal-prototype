package edu.nso.proposals.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
public class UserController {
    @GetMapping("/debug/user")
    public Principal user(Principal principal) {
        return principal;
    }
}
