package edu.nso.proposals.model;

import org.springframework.data.rest.core.config.Projection;

@Projection(name = "inlinePi", types = {Proposal.class})
public interface InlinePi {
    String getTitle();

    Person getPi();

    ProposalState getState();
}
