package edu.nso.proposals.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Singular;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Version;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.GenerationType.AUTO;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@EntityListeners(AuditingEntityListener.class)
public class Proposal {
    @Version
    private Long version;

    @Id
    @GeneratedValue(strategy = AUTO)
    private Long id;
    @CreatedDate
    private Date createDate;
    @LastModifiedDate
    private Date lastModifiedDate;

    @OneToOne(fetch = FetchType.EAGER)
    private Person pi;

    @ManyToMany
    @Singular("copi")
    private Set<Person> copis = new HashSet<>();

    @OneToOne
    private Person alternativeContact;

    @OneToOne
    private Thesis thesis;

    @NotEmpty(message = "Title cannot be empty")
    private String title;

    private String proposalAbstract;
    private Integer revision;
    private ProposalState state = ProposalState.DRAFT;
}
