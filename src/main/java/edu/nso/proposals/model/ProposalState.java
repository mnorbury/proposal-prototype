package edu.nso.proposals.model;

public enum ProposalState {
    DRAFT,
    SUBMITTED,
    ACCEPTED,
    REJECTED
}
