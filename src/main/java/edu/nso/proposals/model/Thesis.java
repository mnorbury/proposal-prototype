package edu.nso.proposals.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Version;
import java.util.Date;

import static javax.persistence.GenerationType.AUTO;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@EntityListeners(AuditingEntityListener.class)
public class Thesis {
    @Version
    private Long version;
    
    @Id
    @GeneratedValue(strategy = AUTO)
    private Long id;
    @CreatedDate
    private Date createDate;
    @LastModifiedDate
    private Date lastModifiedDate;

    @OneToOne
    private Person student;

    @OneToOne
    private Person supervisor;

    private String degree;
    private String title;
    private String description;
}
