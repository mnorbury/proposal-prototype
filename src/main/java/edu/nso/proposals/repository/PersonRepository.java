package edu.nso.proposals.repository;

import edu.nso.proposals.model.Person;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@RepositoryRestResource(path = "person")
@CrossOrigin
public interface PersonRepository extends PagingAndSortingRepository<Person, Long> {
}
