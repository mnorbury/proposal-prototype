package edu.nso.proposals.repository;

import edu.nso.proposals.model.InlinePi;
import edu.nso.proposals.model.Person;
import edu.nso.proposals.model.Proposal;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@RepositoryRestResource(path = "proposal", excerptProjection = InlinePi.class)
@CrossOrigin
public interface ProposalRepository extends PagingAndSortingRepository<Proposal, Long> {
    Page findByPi(@Param("pi") Person pi, Pageable page);
}
