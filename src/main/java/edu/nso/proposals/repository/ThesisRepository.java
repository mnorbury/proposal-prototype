package edu.nso.proposals.repository;

import edu.nso.proposals.model.Thesis;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@RepositoryRestResource(path = "thesis")
@CrossOrigin
public interface ThesisRepository extends PagingAndSortingRepository<Thesis, Long> {
}
